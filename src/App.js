import avatar from "./assets/images/avatar.jpg";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="devcamp-container container">
      <div>
        <img className="devcamp-avatar" src={avatar} width={100} alt="avatar"/>
      </div>
      <div>
        <p className="devcamp-quote">This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
      </div>
      <div>
        <p className="devcamp-info"><b>Tammy Stevens</b> * Front End Developer</p>
      </div>
    </div>
  );
}

export default App;
